#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

  CREATE EXTENSION pgcrypto;

  CREATE TABLE users(
    user_id serial PRIMARY KEY,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL
  );

  INSERT INTO users(user_id, username, password) VALUES (DEFAULT, 'user1', crypt('password1', gen_salt('bf')));
  INSERT INTO users(user_id, username, password) VALUES (DEFAULT, 'user2', crypt('password2', gen_salt('bf')));
  INSERT INTO users(user_id, username, password) VALUES (DEFAULT, 'user3', crypt('password3', gen_salt('bf')));

  SELECT * FROM users

EOSQL
