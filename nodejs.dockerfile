FROM node:latest

WORKDIR /app

COPY ./webapp/package*.json /app

RUN npm install

COPY ./webapp /app

EXPOSE 80

CMD ["node", "server.js"]