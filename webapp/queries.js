const got = require('got')
const Pool = require('pg').Pool
const pool = new Pool({
    user: 'postgres',
    host: 'example-webapp-db',
    database: 'webapp_db',
    password: 'password',
    port: 5432
})

const auth = (request, response) => {
    console.log(request)
    const {username, password} = request.body

    pool.query('SELECT username FROM users WHERE username = $1 AND password = crypt($2, password)', [username, password], (error, results) => {
        if(error) {
            throw error
        }

        // The specific user / password combination was not found. Return an error.
        if (results.rowCount != 1) {
            response.status(401).send({Success: false, Message: "Username or password is incorrect."})
            console.log("Authentication for user %s denied.", username)
            response.send
            return
        }


        // TODO make configurable - perhaps publickey, timeoutsec and allowips is posted from client or stored in a database. API key is a flag or environment variable.
        var options = {
            uri:    'http://10.0.0.180/api/auth',
            method: 'POST',
            responseType: "json",
            json: {
                "apikey": "cPA1J/YR5uTtI5zL6ResfatMrFnmhAIoasaS8bVI3mQ=", 
                "publickey": "AmEP2bvOpfKgIOEqP1ULyqjw4MEFvi3gNZuk4HNp23E=", 
                "allowedips": "10.45.9.2/32", 
                "timeoutsec": 30
            }
        }

        got('http://10.0.0.180/api/auth', options ).then(proxyresponse => {
            if (proxyresponse.body.Status == "OK" && proxyresponse.body.TimeoutSec > 0) {
                response.status(200).send({"Status": "Ok", "TimeoutSec": proxyresponse.body.TimeoutSec})
                console.log("User %s with publickey %s added for %ds.", username, proxyresponse.body.PublicKey, proxyresponse.body.TimeoutSec)
                return
            }
        })
    })
}

module.exports= {
    auth
}