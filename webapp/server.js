const express = require('express')
const http = require('http')
const db = require('./queries')
const server = express()

server.use(express.json());

// Middleware. Appends headers to all responses.
server.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

server.post('/api/auth', db.auth)

server.listen(80,()=>{
    console.log('Node server create on port 80')    
});
